#CREATE DATABASE
CREATE DATABASE `ClaimDB` /*!40100 DEFAULT CHARACTER SET latin1 */;

#CREATE TABLE 
CREATE TABLE `claims` (
  `claims_id` int(11) NOT NULL,
  `patient_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`claims_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `claim_status_code` (
  `claim_status` varchar(4) DEFAULT NULL,
  `claim_status_desc` varchar(45) DEFAULT NULL,
  `claim_sequence` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `defendants` (
  `claim_id` int(11) DEFAULT NULL,
  `defendant_name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `legal_events` (
  `claim_id` smallint(6) DEFAULT NULL,
  `defendant_name` varchar(45) DEFAULT NULL,
  `claim_status` varchar(5) DEFAULT NULL,
  `change_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#INSERT DATA TO DB
INSERT INTO `ClaimDB`.`claims` (`claims_id`, `patient_name`) VALUES ('1', 'Bassem Dghaidi');
INSERT INTO `ClaimDB`.`claims` (`claims_id`, `patient_name`) VALUES ('2', 'Omar Breidi');
INSERT INTO `ClaimDB`.`claims` (`claims_id`, `patient_name`) VALUES ('3', 'Marwan Sawwan');

INSERT INTO legal_events VALUES(1, 'Jean Skaff', 'AP', '2016-01-01'); 
INSERT INTO legal_events VALUES(1, 'Jean Skaff',  'OR', '2016-02-02'); 
INSERT INTO legal_events VALUES(1, 'Jean Skaff',  'SF', '2016-03-01'); 
INSERT INTO legal_events VALUES(1, 'Jean Skaff', 'CL', '2016-04-01'); 
INSERT INTO legal_events VALUES(1, 'Radwan Sameh', 'AP', '2016-01-01'); 
INSERT INTO legal_events VALUES(1, 'Radwan Sameh', 'OR', '2016-02-02'); 
INSERT INTO legal_events VALUES(1, 'Radwan Sameh', 'SF', '2016-03-01'); 
INSERT INTO legal_events VALUES(1, 'Elie Meouchi', 'AP', '2016-01-01'); 
INSERT INTO legal_events VALUES(1, 'Elie Meouchi', 'OR', '2016-02-02'); 
INSERT INTO legal_events VALUES(2, 'Radwan Sameh', 'AP', '2016-01-01'); 
INSERT INTO legal_events VALUES(2, 'Radwan Sameh', 'OR', '2016-02-01'); 
INSERT INTO legal_events VALUES(2, 'Paul Syoufi', 'AP', '2016-01-01'); 
INSERT INTO legal_events VALUES(3, 'Issam Awwad', 'AP', '2016-01-01');

INSERT INTO defendants VALUES (1, 'Jean Skaff');
INSERT INTO defendants VALUES (1, 'Elie Meouchi');
INSERT INTO defendants VALUES (1, 'Radwan Sameh');
INSERT INTO defendants VALUES (2, 'Joseph Eid');
INSERT INTO defendants VALUES (2, 'Paul Syoufi');
INSERT INTO defendants VALUES (2, 'Radwan Sameh');
INSERT INTO defendants VALUES (3, 'Issam Awwad');

INSERT INTO ClaimStatusCodes VALUES('AP', 'Awaiting review panel', 1);
INSERT INTO ClaimStatusCodes VALUES('OR', 'Panel opinion rendered', 2);
INSERT INTO ClaimStatusCodes VALUES('SF', 'Suit filed', 3);
INSERT INTO ClaimStatusCodes VALUES('CL', 'Closed', 4);


#QUERY
SELECT claim_id ,patient_name, claim_status
FROM
(SELECT 
    claim_id, MIN(count_def) as min_claim
FROM
    (SELECT 
        claim_id, COUNT(defendant_name) AS count_def
    FROM
        legal_events
    GROUP BY defendant_name , claim_id
    ORDER BY claim_id) AS temp
GROUP BY claim_id) as mintable
JOIN claims as c ON c.claims_id = claim_id
JOIN claim_status_code as csc ON csc.claim_sequence = min_claim