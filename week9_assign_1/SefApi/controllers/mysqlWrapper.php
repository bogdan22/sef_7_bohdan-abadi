<?php


class database {
    private $conn, $stmt, $arr, $eof=true, $error, $res;

    public function __construct($host, $user, $pass, $db) {
        $this->conn = mysqli_connect($host, $user, $pass, $db);
        if(mysqli_connect_errno()) $this->error = mysqli_connect_error();
    }

    public function __destruct() {
        if($this->stmt) mysqli_stmt_close($this->stmt);
        mysqli_close($this->conn);
    }

    /** Executes a prepared SQL query on the database
      *
      * @param string $query is the sql statement to execute
      * @return mixed false on error,
      *               true on successful select, or
      *               the number of affected rows
      */
    public function query($query) {
        // create a new prepared statement
        $stmt = mysqli_prepare($this->conn, $query);
        if(!$stmt) {
            $this->error = mysqli_error($this->conn);
            return false;
        }

        // apply the arguments if any exist
        if(func_num_args() > 2) {
            $args = array_slice(func_get_args(), 1);
            $refs = array();
            foreach($args as $key=>&$value)
             $refs[$key] = &$value;
            call_user_func_array(array($stmt, 'bind_param'), $refs);
        }

        // run the query
        $stmt->execute();
        if($stmt->errno) {
            $this->error = mysqli_error($this->conn);
            return false;
        }

        // if the query was not a select, return the number of rows affected
        if($stmt->affected_rows > -1) {
            $rows = $stmt->affected_rows;
            mysqli_stmt_close($stmt);
            return $rows;
        }

        $meta = $stmt->result_metadata(); 

        while ($field = $meta->fetch_field()) { 
            $params[] = &$row[$field->name]; 
        } 

        call_user_func_array(array($stmt, 'bind_result'), $params);            
        while ($stmt->fetch()) { 
            foreach($row as $key => $val) { 
                $c[$key] = $val; 
            } 
            $hits[] = $c; 
        }
        $stmt->close();
        $this->res = $hits;
        return true;

    }



    public function results() {
        return $this->res;
    }

    // return the value for the specified field
    public function result($field) 
    { 
      return $this->arr[$field]; 
    }
    public function __get($field) 
    { 
      return $this->result($field); 
    }

    // returns true if eof has occured or an error
    public function eof() 
    { 
      return $this->eof; 
    }

    // returns the number of rows in the result set
    public function count() 
    { 
      return ($this->stmt ? $this->stmt->num_rows : 0); 
    }

    // returns the last error message, if clear is true, clears the error as well
    public function error($clear=false) {
        $err = $this->error;
        if($clear) $this->error = '';
        return $err;
    }
}
?>