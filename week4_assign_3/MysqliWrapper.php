<?php
session_start();

Class MysqliWrapper	{
	//require_once "config.php";
	private $dbCon;
	private $storeID;
	private $customerID;
	
	function __construct()
	{

	}

	function connect()
	{
		require_once "config.php";
		$this -> dbCon = new mysqli($config['hostname'], $config['dbuser'],
												$config['dbpassword'],$config['dbname']);
		if($this -> dbCon-> connect_error) {
			print_r("Couldn't connect to Database!");
		}
		//else  {
		//	print_r("We're connected to: ".$config['dbname']);
		//}
	}

	function getUser($email)
	{
		$query = "SELECT 
							    store_id, customer_id, email, first_name , last_name
							FROM
							    customer
							WHERE
							     email = '$email'";

		$result = $this -> dbCon -> query($query);
		$row = $result -> num_rows;	

		if($row <0) {
			return "No user found";
		}	
		else {
			$user = $result -> fetch_array(MYSQLI_ASSOC);
			$_SESSION ['customerid'] = $user['customer_id'];
			$_SESSION['customername'] = $user['first_name'] . " ". $user['last_name'];
			return $user['store_id'];
		}

	}

	function availableFilm($storeID)
	{
		$this -> storeID = $storeID;
		$filmArray = array();

		$query = "SELECT 
							    F.title
							FROM
							    film AS F
							        JOIN
							    inventory AS inv ON inv.film_id = F.film_id
							        AND inv.store_id = $storeID
							        LEFT JOIN
							    (SELECT 
							        INV.inventory_id AS InventoryId, RENT.return_date
							    FROM
							        inventory AS INV
							    JOIN rental AS RENT ON RENT.return_date IS NULL
							        AND RENT.inventory_id = INV.inventory_id
							    WHERE
							        INV.store_id = $storeID) AS S ON S.InventoryId = inv.inventory_id
							WHERE
							    S.InventoryId IS NULL
							GROUP BY F.title";

		$result = $this -> dbCon -> query($query);
		$row = $result -> num_rows;
		for($i = 0; $i < $row;++$i) {
			$result -> data_seek($i);
			$row= $result -> fetch_array(MYSQLI_ASSOC);
			$filmArray []= $row['title'];
		}
			return $filmArray;
	}

	function getFilmInv($filmName , $storeID) {
			
			$query = "SELECT 
								    inv.inventory_id
								FROM
								    film AS F
								        JOIN
								    inventory AS inv ON inv.film_id = F.film_id
								        AND inv.store_id = $storeID
								        LEFT JOIN
								    (SELECT 
								            INV.inventory_id AS InventoryId,
								            RENT.return_date
								    from inventory as INV
								    JOIN rental AS RENT ON RENT.return_date IS NULL
								        AND RENT.inventory_id = INV.inventory_id
								        where INV.store_id = $storeID) AS S ON S.InventoryId = inv.inventory_id
								WHERE
								    S.InventoryId IS NULL AND F.title = '$filmName'
								    limit 1";

			$result = $this -> dbCon -> query($query);

			$filmID = $result -> fetch_array(MYSQLI_ASSOC);

			return $filmID['inventory_id'];
	}

	function rentFIlm($invID, $customerID , $staffID)
	{
		$query = "INSERT INTO rental(rental_date, inventory_id, customer_id, staff_id) 
    					VALUES(NOW(), $invID, $customerID , $staffID)";

    if($this -> dbCon -> query($query)) {
    	print_r("INSERT SUCCESS");
		}
		else{
			print_r("FAILED QUERY");
		}
	}


	function disconnect(){
		mysqli_close($this -> dbCon);
	}

}
?>

