import boto3
access_key = "AKIAJKSZZBUTSPRWCGEQ"
access_secret = "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"
region ="us-east-1"
queue_url = "https://sqs.us-east-1.amazonaws.com/150904494424/AlexaPiScripts"

def build_speechlet_response(title, output, reprompt_text, should_end_session):
    return {
        'outputSpeech': {
            'type': 'PlainText',
            'text': output
        },
        'card': {
            'type': 'Simple',
            'title': "SessionSpeechlet - " + title,
            'content': "SessionSpeechlet - " + output
        },
        'reprompt': {
            'outputSpeech': {
                'type': 'PlainText',
                'text': reprompt_text
            }
        },
        'shouldEndSession': should_end_session
    }

def build_response(session_attributes, speechlet_response):
    return {
        'version': '1.0',
        'sessionAttributes': session_attributes,
        'response': speechlet_response
    }

def post_message(client, message_body, url):
    response = client.send_message(QueueUrl = url, MessageBody= message_body)
    
def lambda_handler(event, context):
    client = boto3.client('sqs', aws_access_key_id = access_key, aws_secret_access_key = access_secret, region_name = region)
    intent_name = event['request']['intent']['name']
    if intent_name == "MirrorOn":
        post_message(client, 'on', queue_url)
        message = "On"
    elif intent_name == "MirrorOff":
        post_message(client, 'off', queue_url)
        message = "off"
    elif intent_name == "ClockOff":
        post_message(client, 'Clock Off', queue_url)
        message = "CLock Off"
    elif intent_name == "ClockOn":
        post_message(client, 'Clock On', queue_url)
        message = "Clock On"
    elif intent_name == "WeatherOff":
        post_message(client, 'Weather Off', queue_url)
        message = "Weather Off"
    elif intent_name == "WeatherOn":
        post_message(client, 'Weather On', queue_url)
        message = "Weather On"
    elif intent_name == "NewsOff":
        post_message(client, 'News off', queue_url)
        message = "News Off"
    elif intent_name == "NewsOn":
        post_message(client, 'News On', queue_url)
        message = "News On"      
    else:
        message = "Unknown"
        
    speechlet = build_speechlet_response("Mirror Status", message, "", "true")
    return build_response({}, speechlet)
