import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Rx';

@Component({
    selector: 'clock',
    templateUrl: 'app/clock/clock.component.html',
		styleUrls: ['app/clock/clock.component.css']
})
export class ClockComponent {
	private days =['Sun',
								 'Mon',
								 'Tue',
								 'Wed',
								 'Thu',
								 'Fri',
								 'Sat' 
							];			
	private clock = Observable
			.interval(1000)
			.map(()=> new Date());
}
        