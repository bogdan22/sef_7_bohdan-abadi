"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var weather_service_1 = require("./weather.service");
var WeatherComponent = (function () {
    function WeatherComponent(weatherService) {
        this.weatherService = weatherService;
        console.log('test');
    }
    WeatherComponent.prototype.getWeather = function () {
        var _this = this;
        this.weatherService.getLocation()
            .subscribe(function (result) {
            _this.location = result,
                function (error) { return _this.errorMessage = error; },
                console.log(_this.location),
                _this.weatherService.getWeather(_this.location)
                    .subscribe(function (result) {
                    _this.weather = result,
                        function (error) { return _this.errorMessage = error; },
                        _this.filterWeather(_this.weather);
                });
        });
    };
    WeatherComponent.prototype.filterWeather = function (weather) {
        //console.log(weather);
        this.currentWeather = Object.assign({}, this.weather['currently']);
        this.dailyWeather = Object.assign({}, this.weather['daily']);
        this.hourlyWeather = Object.assign({}, this.weather['hourly']);
        console.log(this.currentWeather);
        console.log(this.dailyWeather);
        console.log(this.hourlyWeather);
        console.log(this.weather);
    };
    WeatherComponent.prototype.ngOnInit = function () {
        this.getWeather();
    };
    return WeatherComponent;
}());
WeatherComponent = __decorate([
    core_1.Component({
        selector: 'weather',
        templateUrl: 'app/weather/weather.component.html',
        styleUrls: ['app/weather/weather.component.css'],
        providers: [weather_service_1.WeatherService]
    }),
    __metadata("design:paramtypes", [weather_service_1.WeatherService])
], WeatherComponent);
exports.WeatherComponent = WeatherComponent;
//# sourceMappingURL=weather.component.js.map