"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/toPromise");
var Observable_1 = require("rxjs/Observable");
var WeatherService = (function () {
    function WeatherService(http) {
        this.http = http;
        this.locationUrl = 'http://ipinfo.io/json'; // URL to web api sources
        this.weatherUrl = 'https://api.darksky.net/forecast/69cadf531aa70770f8e8b2a60fa41b30/';
    }
    //Get the current llcation through the IP address
    WeatherService.prototype.getLocation = function () {
        return this.http.get(this.locationUrl)
            .map(this.extractData)
            .catch(this.handleError);
    };
    //Return a weather request
    WeatherService.prototype.getWeather = function (location) {
        return this.http.get(this.weatherUrl + location.loc + '?&units=si')
            .map(this.extractData)
            .catch(this.handleError);
    };
    WeatherService.prototype.extractData = function (res) {
        this.body = res.json();
        return this.body || {};
    };
    WeatherService.prototype.handleError = function (error) {
        var errMsg;
        if (error instanceof http_1.Response) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable_1.Observable.throw(errMsg);
    };
    return WeatherService;
}());
WeatherService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], WeatherService);
exports.WeatherService = WeatherService;
//# sourceMappingURL=weather.service.js.map