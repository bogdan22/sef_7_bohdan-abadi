import { Injectable }    from '@angular/core';
import { Headers, Http, Response } from '@angular/http';

import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Observable';

import { Location } from './location';



@Injectable()
export class WeatherService {
  errorMessage: string;
  body;
  private locationUrl = 'http://ipinfo.io/json'; // URL to web api sources
  private weatherUrl = 'https://api.darksky.net/forecast/69cadf531aa70770f8e8b2a60fa41b30/';
  constructor(private http: Http) {
  }
  //Get the current llcation through the IP address
  getLocation(): Observable<Location> {
    return this.http.get(this.locationUrl)
                    .map(this.extractData)
                    .catch(this.handleError);                              
  }

  //Return a weather request
  getWeather(location: Location): Observable<Object> {
    return this.http.get(this.weatherUrl+location.loc+'?&units=si')
                    .map(this.extractData)
                    .catch(this.handleError);                              
  }

  private extractData(res: Response) {
    this.body = res.json();       
    return this.body || { };
  }

  private handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } 
    else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}