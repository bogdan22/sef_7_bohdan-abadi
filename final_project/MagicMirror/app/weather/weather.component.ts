import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Rx';

import { WeatherService } from './weather.service';
import {Location} from './location'

@Component({
    selector: 'weather',
    templateUrl: 'app/weather/weather.component.html',
		styleUrls: ['app/weather/weather.component.css'],
    providers: [WeatherService]
})
export class WeatherComponent implements OnInit {
  errorMessage : String;
  location: Location;
  weather: Object;
  currentWeather: Object;
  dailyWeather: Object[];
  hourlyWeather: Object[];

  constructor(private weatherService: WeatherService) {
    console.log('test');

  }

  getWeather() {
    this.weatherService.getLocation()
                       .subscribe(result => {this.location = result,
                                  error => this.errorMessage = <any>error,
                                  console.log(this.location),
                                  this.weatherService.getWeather(this.location)
                                      .subscribe(result => {this.weather = result,
                                                 error => this.errorMessage = <any>error,
                                                 this.filterWeather(this.weather);
                                    });
                    });
  }

  filterWeather(weather: Object){
    //console.log(weather);
    this.currentWeather = Object.assign({}, this.weather['currently']);
    this.dailyWeather = Object.assign({},this.weather['daily']);
    this.hourlyWeather = Object.assign({},this.weather['hourly']);
    console.log(this.currentWeather);
    console.log(this.dailyWeather);
    console.log(this.hourlyWeather);
    console.log(this.weather)
  }


    ngOnInit() {
      this.getWeather();
    }
}
        