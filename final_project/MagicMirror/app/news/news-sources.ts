export class NewsSources {
  id: number;
  name: string;
  description: string;
  url: string;
  category: string;
  language: string;
  country: string;
  urlsToLogos: Object;
  sortBysAvailable: string [];
}
