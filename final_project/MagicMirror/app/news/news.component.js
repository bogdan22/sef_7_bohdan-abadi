"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var news_service_1 = require("./news.service");
var NewsComponent = (function () {
    function NewsComponent(newsService) {
        this.newsService = newsService;
        this.element = [];
    }
    NewsComponent.prototype.getNewsSources = function () {
        var _this = this;
        this.newsService.getNewsSources()
            .subscribe(function (result) {
            _this.newsSources = result,
                function (error) { return _this.errorMessage = error; },
                _this.newsService.getNews(_this.newsSources)
                    .subscribe(function (result) {
                    _this.news = result,
                        function (error) { return _this.errorMessage = error; },
                        _this.showNewsFeed(_this.news);
                });
        });
    };
    NewsComponent.prototype.showNewsFeed = function (news) {
        //Iterate through the array of Objects and filter
        for (var i = 0; i < news.length; i++) {
            var length_1 = news[i]['length'];
            for (var j = 0; j < length_1; j++) {
                this.element.push(news[i][j]);
            }
        }
        this.displayNews(this.element);
    };
    //Get each element to display on HTML
    NewsComponent.prototype.displayNews = function (news) {
        var _this = this;
        var i = 1;
        console.log(news[0]);
        this.showNews = news[0];
        console.log(this.showNews);
        // setInterval makes it run repeatedly
        // get the item and increment i to move to the next
        // reset to first element if you've reached the end
        setInterval(function () {
            _this.showNews = news[i++];
            if (i == news.length)
                i = 0;
        }, 25000);
    };
    NewsComponent.prototype.ngOnInit = function () {
        //Initiat News Requests
        this.getNewsSources();
    };
    return NewsComponent;
}());
NewsComponent = __decorate([
    core_1.Component({
        selector: 'news',
        templateUrl: 'app/news/news.component.html',
        styleUrls: ['app/news/news.component.css'],
        providers: [news_service_1.NewsService]
    }),
    __metadata("design:paramtypes", [news_service_1.NewsService])
], NewsComponent);
exports.NewsComponent = NewsComponent;
//# sourceMappingURL=news.component.js.map