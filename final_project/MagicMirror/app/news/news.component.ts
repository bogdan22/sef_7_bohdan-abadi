import { Component, OnInit } from '@angular/core';
import { Http, Response } from '@angular/http'
import { Observable } from 'rxjs/Rx';

import { NewsSources } from './news-sources';
import { NewsService } from './news.service';
import { News } from './news';

@Component({
  selector: 'news',
  templateUrl: 'app/news/news.component.html',
  styleUrls: ['app/news/news.component.css'],
  providers: [NewsService]
})
export class NewsComponent implements OnInit {
  errorMessage: string;
  newsSources: NewsSources[];
  news: Object[];
  showNews: Object;
  element: Object[];
  length: number;

  constructor(private newsService: NewsService) {                  
    this.element = [];
  }

  getNewsSources() {
    this.newsService.getNewsSources()
                    .subscribe(result => {this.newsSources = result,
                               error => this.errorMessage = <any>error,
                               this.newsService.getNews(this.newsSources)
                                   .subscribe(result => {this.news = result,
                                              error => this.errorMessage = <any>error,
                                              this.showNewsFeed(this.news)
                                  });
                    });
  }

  showNewsFeed(news: Object[]) {
    //Iterate through the array of Objects and filter
    for (var i = 0; i < news.length; i++) {
      let length = news[i]['length'];
      for (var j = 0; j < length; j++) {
        this.element.push(news[i][j]);
      }
    }
    this.displayNews(this.element);
  }

  //Get each element to display on HTML
  displayNews(news: Object[]): void {
    var i = 1;
    console.log(news[0]);
    this.showNews = news[0];
    console.log(this.showNews);
     // setInterval makes it run repeatedly
     // get the item and increment i to move to the next
     // reset to first element if you've reached the end
    setInterval(() => {
      this.showNews = news[i++];    
      if (i == news.length) i = 0;   
    }, 25000);
  }



  ngOnInit() {
    //Initiat News Requests
    this.getNewsSources();
  }

}
