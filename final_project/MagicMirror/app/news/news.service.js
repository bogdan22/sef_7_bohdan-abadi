"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/toPromise");
var Observable_1 = require("rxjs/Observable");
var NewsService = (function () {
    function NewsService(http) {
        this.http = http;
        this.newsSourceUrl = ' https://newsapi.org/v1/sources?language=en&sortBysAvailable=latest&apiKey=39a9318c82214195a962d44702f4a0ae'; // URL to web api sources
    }
    //Get all the availabe news sources
    NewsService.prototype.getNewsSources = function () {
        return this.http.get(this.newsSourceUrl)
            .map(this.extractSources)
            .catch(this.handleError);
    };
    //return the sources from the object response
    NewsService.prototype.extractSources = function (res) {
        this.body = res.json();
        return this.body.sources || {};
    };
    //Gets the news article based on the available sources
    NewsService.prototype.getNews = function (sourcesList) {
        var observableBatch = [];
        //Fills an array of available sources
        for (var _i = 0, sourcesList_1 = sourcesList; _i < sourcesList_1.length; _i++) {
            var source = sourcesList_1[_i];
            var newsUrl = "https://newsapi.org/v1/articles?source=" + source.id + "&apiKey=39a9318c82214195a962d44702f4a0ae"; //URL to web api Articles
            observableBatch.push(this.http.get(newsUrl)
                .map(this.extractArticles)
                .catch(this.handleError));
        }
        return Observable_1.Observable.forkJoin(observableBatch);
    };
    NewsService.prototype.extractArticles = function (res) {
        this.body = res.json();
        return this.body.articles || {};
    };
    NewsService.prototype.handleError = function (error) {
        var errMsg;
        if (error instanceof http_1.Response) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable_1.Observable.throw(errMsg);
    };
    return NewsService;
}());
NewsService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], NewsService);
exports.NewsService = NewsService;
//# sourceMappingURL=news.service.js.map