import { Injectable }    from '@angular/core';
import { Headers, Http, Response } from '@angular/http';

import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Observable';


import { NewsSources } from './news-sources';
import { News } from './news';

@Injectable()
export class NewsService {
  errorMessage: string;
  newsSource : NewsSources;
  body;
  private newsSourceUrl = ' https://newsapi.org/v1/sources?language=en&sortBysAvailable=latest&apiKey=39a9318c82214195a962d44702f4a0ae'; // URL to web api sources
  
  constructor(private http: Http) {
  }
  //Get all the availabe news sources
   getNewsSources(): Observable<NewsSources[]> {
    return this.http.get(this.newsSourceUrl)
                  .map(this.extractSources)
                  .catch(this.handleError);                              
  }
  //return the sources from the object response
  private extractSources(res: Response) {
    this.body = res.json();       
     return this.body.sources || { };
  }
  //Gets the news article based on the available sources
  getNews(sourcesList: NewsSources []): Observable<Object[]>{
    let observableBatch = [];
    //Fills an array of available sources
    for(let source of sourcesList )  {
       let newsUrl = `https://newsapi.org/v1/articles?source=${source.id}&apiKey=39a9318c82214195a962d44702f4a0ae`; //URL to web api Articles
       observableBatch.push( this.http.get(newsUrl)
                              .map(this.extractArticles)
                              .catch(this.handleError));
    }
    return  Observable.forkJoin(observableBatch); 
  }

  private extractArticles(res: Response) {
  this.body = res.json();
  return <News[]> this.body.articles || { };
  }

  private handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } 
    else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
