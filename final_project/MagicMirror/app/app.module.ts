import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule }    from '@angular/http';

import { ClockComponent }   from './clock/clock.component';

import { NewsComponent }   from './news/news.component';
import { NewsService }          from './news/news.service';

import { WeatherComponent }   from './weather/weather.component';
import { WeatherService }   from './weather/weather.service';

@NgModule({
  imports:      [ BrowserModule,
                  HttpModule
                ],
  declarations: [ ClockComponent,
                  NewsComponent,
                  WeatherComponent
                ],
  providers:    [ NewsService,
                  WeatherService
                ],               
  bootstrap:    [ ClockComponent,
                  NewsComponent,
                  WeatherComponent
                ]
})
export class AppModule { }