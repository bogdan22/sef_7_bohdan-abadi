<?php
// Directory for the git Log
$dir = "/home/bogdan/GitPunch/";

//Change working directory
chdir($dir);

//Exec to get the change log wtih specific information and insert insert into string array
exec("git log --pretty=format:'%H - %an - %ad - %s %ae %ct'",$output);
//Global Variables
$matches;
$options = getopt("a:e:t:d:m:s:");
$output = array();
$finalResult =array();
$counter = 0;

//If a option is chosen by the user search by name
if(isset($options['a'])) {
	foreach ($output as $result) {
			preg_match('/' . $options['a'].'/', $result, $match);
			if (count($match) > 0) {
				$finalResult[$counter] = $result;
				$counter ++;
				}	
			}
	$counter = 0;
}

//If e option is chosen by the user search by email
if(isset($options['e'])) {
	if(!empty($finalResult)){
		$output = $finalResult;
		unset($finalResult);
	}
	foreach ($output as $result) {
		preg_match('/' . $options['e'].'/', $result, $match);
		if (count($match) > 0) {
			$finalResult[$counter] = $result;
			$counter ++;
		}
	}
	$counter = 0;
}

//If t option is chosen by the user search by time HH:MM
if(isset($options['t'])) {
	if(!empty($finalResult)){
		$output = $finalResult;
		unset($finalResult);
	}
	foreach ($output as $result) {
		preg_match('/' . $options['t'].':[0-9]+'.'/', $result, $match);
		if (count($match) > 0) {
			$finalResult[$counter] = $result;
			$counter ++;
		}
	}
	$counter = 0;
}

//If s option is chosen by the user search by D D-M-Y
if(isset($options['d'])) {
	if(!empty($finalResult)){
		$output = $finalResult;
		unset($finalResult);
	}
	$datetime = DateTime::createFromFormat('l-m-Y', $options['d']);
	foreach ($output as $result) {
		preg_match('/' . $datetime->format('D M') . ' [0-9]+ [0-9]+:[0-9]+:[0-9]+ '. $datetime->format('Y') .'/',$result, $match);
		if (count($match) > 0) {
			$finalResult[$counter] = $result;
			$counter ++;
		}
	}
	$counter = 0;
}

//If m option is chosen by the user search by commit message
if(isset($options['m'])) {
	if(!empty($finalResult)){
		$output = $finalResult;
		unset($finalResult);
	}
	foreach ($output as $result) {
		preg_match('/' . $options['m'].'/', $result, $match);
		if (count($match) > 0) {
			$finalResult[$counter] = $result;
			$counter ++;
		}
	}
	$counter = 0;
}

//If s option is chosen by the user search by Epoch time (Seconds)
if(isset($options['s'])) {
	if(!empty($finalResult)){
		$output = $finalResult;
	}
	foreach ($output as $result) {
		preg_match('/' . $options['s'].'/', $result, $match);
		if (count($match) > 0) {
			$finalResult[$counter] = $result;
			$counter ++;
		}
	}
	$counter=0;
}

$displayMessage = "Search Result by ";

//Search for options that were selected and insert a message accordingly to each option
foreach ($options as $key => $value) {
	$myKey = $key;
	switch ($myKey) {
		case 'a':
			$displayMessage .="Author ". $options['a']. " - ";
			break;

		case 'e':
			$displayMessage .="Email ". $options['e']. " - ";
			break;

		case 't':
			$displayMessage .="Time ". $options['t']." - ";
			break;

		case 'd':
			$displayMessage .="Date " .$options['d']." - ";
			break;	

		case 's':
			$displayMessage .="Epoch  TimeStamp ".$options['s']." - ";
			break;					

		case 'm':
			$displayMessage .="Date ".$options['m']." - ";
			break;				
		
	}	
}
//Display the message with the amount of results
print($displayMessage). "Total: " .sizeof($finalResult) ."\n";

//Delete the last 2 words and display the search result
foreach ($finalResult as $search ) {
	$str = $search;
	$words = explode( " ", $str );
	array_splice( $words, -2 );
	print(implode(" ", $words)."\n");
}

?>