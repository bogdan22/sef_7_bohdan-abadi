<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;

class APIController extends Controller
{
    //
     public function store()
    {
        $message = new Message;

        $message->user_id = request('user_id');
        $message->message = request('message');

        $message->save();

    }
}
