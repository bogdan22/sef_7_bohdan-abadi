@extends('layouts.app')

@section('content')


<script src="https://use.fontawesome.com/45e03a14ce.js"></script>

<div class="main_section" onload="init()">
   <div class="container">
      <div class="chat_container">
         <div class="col-sm-3 chat_sidebar">
         <div class="row">
            <div id="custom-search-input">
               <div class="input-group col-md-12">
                  <input type="text" class="  search-query form-control"
                   placeholder="Conversation" />
                  <button class="btn btn-danger" type="button">
                  <span class=" glyphicon glyphicon-search"></span>
                  </button>
               </div>
            </div>
            <div class="dropdown all_conversation">
               <button class="dropdown-toggle" type="button" id="dropdownMenu2"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
               <i class="fa fa-weixin" aria-hidden="true"></i>
               All Conversations
               <span class="caret pull-right"></span>
               </button>
               <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                  <li><a href="#"> All Conversation </a>
                  <ul class="sub_menu_ list-unstyled">
                    <li><a href="#"> All Conversation </a> </li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li><a href="#">Separated link</a></li>
               </ul>
               </li>
                  <li><a href="#">Another action</a></li>
                  <li><a href="#">Something else here</a></li>
                  <li><a href="#">Separated link</a></li>
               </ul>
            </div>
            <div class="member_list">
               <ul class="list-unstyled">
                  <li class="left clearfix">
                     <span class="chat-img pull-left">
                     <img src="https://lh6.googleusercontent.com/-y-MY2satK-E/AAAAAAAAAAI/AAAAAAAAAJU/ER_hFddBheQ/photo.jpg" alt="User Avatar" class="img-circle">
                     </span>
                     <div class="chat-body clearfix">
                        <div class="header_sec">
                           <strong class="primary-font">Jack Sparrow</strong>
                            <strong class="pull-right">
                           09:45AM</strong>
                        </div>
                        <div class="contact_sec">
                           <strong class="primary-font">(123) 123-456</strong> 
                           <span class="badge pull-right">3</span>
                        </div>
                     </div>
                  </li>
                  
               </ul>
            </div></div>
         </div>
         
         <!--chat_sidebar-->
         <div class="col-sm-9 message_section">
           <div class="row">
             <div class="new_message_head">
               <div class="status">
                 <div id="current-status">
                    <p>Status: </p>
                 </div>
               </div>
               <div class="pull-left"><button><i class="fa fa-plus-square-o" aria-hidden="true">
               </i> New Message</button>
               </div>
               <div class="pull-right">
                 
                 <div class="dropdown">
                  <button class="dropdown-toggle" type="button" id="dropdownMenu1"
                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-cogs" aria-hidden="true"></i>  Setting
                    <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Profile</a></li>
                    <li><a href="#">Logout</a></li>
                  </ul>
                  </div>
                </div>
             </div><!--new_message_head-->
           <div class="chat_area" id="chat-list" >
           <ul class="list-unstyled" id="chat">
                        @foreach ($messages as $message)
                       <li class="left clearfix">
                         <span class="chat-img1 pull-left">
                         <img src="https://lh6.googleusercontent.com/-y-MY2satK-E/AAAAAAAAAAI/AAAAAAAAAJU/ER_hFddBheQ/photo.jpg" alt="User Avatar" class="img-circle">
                         </span>
                         <div class="chat-body1 clearfix">
                            <p><b>{{$message->name}}:</b> <br/> {{$message->message}}</p>
                            <div class="chat_time pull-right">{{$message->created_at}}</div>
                         </div>
                       </li>
                      @endforeach
           </ul>
           </div><!--chat_area-->
            <div class="message_write">
               <textarea class="form-control" id="message" placeholder="type a message">
               </textarea>
               <div class="clearfix"></div>
                   <div class="chat_bottom"><a href="#" class="pull-left upload_btn">
                   <i class="fa fa-cloud-upload" aria-hidden="true"></i>
                   Add Files</a>
                   <a href="#" id="sendButton" class="pull-right btn btn-success">Send
                   </a>
                   
                   </div>
           </div>
           </div>
         </div> <!--message_section-->
      </div>
   </div>
</div>全
<script src="http://code.jquery.com/jquery-1.11.0.min.js">
</script>

<script type="text/javascript">

function updateScroll()
   {    
        var element = document.getElementById("chat-list");
        element.scrollTop = element.scrollHeight;
   }

  var socket;

  function createSocket(host) {

      if ('WebSocket' in window)
          return new WebSocket(host);
      else if ('MozWebSocket' in window)
          return new MozWebSocket(host);

      throw new Error("No web socket support in browser!");
  }

  function init() {
      var host = "ws://localhost:12345/chat";
      try {
          socket = createSocket(host);
          if(socket.readyState == 0) {
          document.getElementById('current-status').innerHTML = '<div class="loader"></div>';
          }

          socket.onopen = function(msg) {
              //log("Welcome - status " + this.readyState);

              document.getElementById('current-status').innerHTML = '<p>Connect</p>\n'+
                    '<img src={{url('icons/green_status.png')}} class="img-circle" alt="online" width="14" height="12">';
          };
          socket.onmessage = function(msg) {
              log(msg.data);
          };
          socket.onclose = function(msg) {
              //log("Disconnected - status " + this.readyState);
              document.getElementById('current-status').innerHTML = '<p>Disconnected</p>\n'+
                    '<img src={{url('icons/red_status.png')}} class="img-circle" alt="online" width="14" height="12">';
          };
      }
      catch (ex) {
          log(ex);
      }
      document.getElementById("message").focus();
  }

  function send() {
    var jsonMessage = {
              id: "{{Auth::user()->id}}",
              name: "{{Auth::user()->name}}",
              message: document.getElementById('message').value
            };

    var message = document.getElementById('message').value;
    //var msg = message;

      try {
          socket.send(JSON.stringify(jsonMessage));
      } catch (ex) {
          log(ex);
      }
  }
  function quit() {
      log("Goodbye!");
      socket.close();
      socket = null;
  }

  function log(msg) {
      document.getElementById("chat").innerHTML += '<li class="left clearfix">\n'+
                                                    '<span class="chat-img1 pull-left">\n'+
                                                   '<img src="https://lh6.googleusercontent.com/-y-MY2satK-E/AAAAAAAAAAI/AAAAAAAAAJU/ER_hFddBheQ/photo.jpg" alt="User Avatar" class="img-circle">\n'+
                                                   '</span>\n'+
                                                   '<div class="chat-body1 clearfix">\n'+
                                                   '<p>'+msg+'</p>\n'+
                                                   '<div class="chat_time pull-right">09:40PM</div>\n'+
                                                   '</div>\n'+
                                                   '</li>\n';

                      
  }

  function onkey(event) {
      if (event.keyCode == 13) {
          send();
      }
  }

window.addEventListener('load',function(){

   

  updateScroll();

  var el = document.getElementById("sendButton");
  el.addEventListener("click", sendButtonClicked, false);

  function sendButtonClicked () 
  {
    updateScroll();
    send();
    document.getElementById('message').value = "";
  }
});
init();

</script>
@endsection
