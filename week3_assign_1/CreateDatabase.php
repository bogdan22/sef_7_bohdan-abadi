<?php

class CreateDatabase {

	private $dbName;
	private $tbName;
	//CreateDtabase Constructor creates folder if it does not exist and change the directory
	function __construct() {
		if (!file_exists("Database")) {
				mkdir("Database/", 0777, true);    	
		}
		chdir("Database/");
	}
	//Select existing Database ,insert name into dbName to be used as a directory path later
	function selectDB($dbName) {
		$flag = false;
		$findDB = scandir("../Database");
		for($i =0 ; $i<count($findDB); $i++) {
			if($findDB[$i] == $dbName) {
				$this -> dbName = $dbName;
				$flag = true;
				print_r("SELECTED DATABASE: " . $this -> dbName . "\n");
				break;
			}
		}
		if($flag == false){
			print_r("TABLE ".$dbName ." DOESN'T EXIST! \n");
		}
	}
	//Select existing table , insert name into tbName to be used as a file path
	function selectTable($tbName) {
		$flag = false;
		$findTable = scandir($this -> dbName);
		for($i =0 ; $i<count($findTable); $i++) {
			if($findTable[$i] == $tbName .".csv") {
				$this -> tbName = $tbName;
				$flag = true;
				print_r("SELECTED TABLE: " . $this -> tbName . "\n");
				break;
			}
		}
		if($flag == false){
			print_r("TABLE ".$tbName ." DOESN'T EXIST! \n");
		}
	}
	//Create database and change the directory name
	function createDB($dbName) {
		if (!file_exists($this-> dbName)) {
			$this -> dbName = $dbName;
    	mkdir($this -> dbName, 0777, true);
    	print_r("Database was Created \n");
		}
		else {
			throw new Exception('Database Already Exist.');
			    	echo "Database Not Created";

		}
	}
	//Check wether the DB name inserted by the user exists and delete it
	function deleteDB($dbName) {
		$tempCurrentDB = $this -> dbName ;
		$this -> dbName = $dbName;
		if(file_exists($this-> dbName)) {
			rmdir($this -> dbName);
			print_r("DELETED DATABASE: " . $this -> dbName . "\n");

		}
		else {
			throw new Exception('Database Doest Not Exist or Not Selected!');
			    	echo "No Database Was Deleted";
		}
		$this -> dbName = $tempCurrentDB;
	}

		//Add table to an existing DB
		function addTable($columnArray) {
			$this -> tbName = $columnArray[2];
			for($i=0; $i<4; $i++) {
				array_shift($columnArray);
			}
			if(($file = fopen($this -> dbName."/".$this -> tbName . '.csv', 'w')) !==FALSE)	{
				fputcsv($file,$columnArray);
				fclose($file);
				print_r("TABLE ADDED: " . $this -> tableName . "\n");
			}
			else{
				throw new Exception("Error creating table, Database maybe not valid!");
			}
		}
		//Add record to an existing table
		function addRecord($recordArray) {
			array_shift($recordArray);
			if(($file = fopen($this -> dbName."/".$this -> tbName . '.csv', 'a') !== FALSE)) {
			fputcsv($file,$recordArray);
			fclose($file);
			print_r("RECORD ADD INTO: " . $this -> tbName . "\n");
			}
			else{
				throw new Exception("The Table Doesn't Exists!");
			}
		}
		//Gets the rows that matches a column value with the input from the user
		function getRecord ($record) {
			array_shift($record);
			if (($file = fopen($this -> dbName."/".$this -> tbName . '.csv', "r")) !== FALSE) {
    		while (($data = fgetcsv($file, 10000, ",")) !== FALSE) {
        	$num = count($data);
        	for ($c=0; $c < $num; $c++) {
        		if($data[$c] == $record[0]) {
        			for($i=0; $i<$num; $i++) {
        				if(count($data)-1 != $i) {
        				print_r($data[$i]. " ,");
        				}
        				else {
        				print_r($data[$i]);
        				}
        			}
            }
        	}
        	echo "\n";
    		}
    	fclose($file);
			}
		}
		//Delete a record by copying and skipping the value that is matched
		function deleteRecord($record) {
			$newArray = array();
			$fTemp = fopen($this -> tbName."temp.csv", "a+");
			if (($file = fopen($this -> dbName."/".$this -> tbName . '.csv', "r")) !== FALSE) {
    			while (($data = fgetcsv($file, 10000, ",")) !== FALSE) {
        		$num = count($data);
        		if($data[0] != $record){
        			print_r($data);
        			fputcsv($fTemp, $data);;
        		}
        		else{
        			continue;
    				}
    				print_r("Record Deleted \n");
					}
					fclose($file);
					fclose($fTemp);
					unlink($this -> dbName."/".$this -> tbName.".csv");
					rename($this -> tbName."temp.csv",$this -> dbName."/". $this -> tbName .".csv");
			}
			else{
				throw new Exception("Table ".$this -> tbName ." was not found");
			}
		}

	 	function __destruct() {
  	echo 'Destroying: ', $this->dbName . "\n";
  }
}

?>