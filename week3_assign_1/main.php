<?php
require_once "CreateDatabase.php";
//Read Input from the user
$line = readline("Commands \n ");
//Create Object "CreateDatabase"
$createdDatabase = new CreateDatabase();
													
//Loop until the user enters "Exit"																					
while(strcasecmp($line, 'exit') != 0) {
		//Select existing database get input and call selectDB() function
		if(fnmatch("SELECT,DATABASE,*", $line)) {
			$pieces= explode(',', $line);
			$dbName = $pieces[2];
			$createdDatabase -> selectDB($dbName);
			
		}
		//Select existing table get the input and call selectTable() function
		else if(fnmatch("SELECT,TABLE,*", $line)) {
			$pieces= explode(',', $line);
			$tableName = $pieces[2];
			$createdDatabase -> selectTable($tableName);
		}
		//Create A new Database get the input and call the createdDB function
		else if(fnmatch("CREATE,DATABASE,*", $line)) {
			$pieces = explode(',', $line);
			$command = array_pop($pieces);
			print_r($command . "\n");
			$createdDatabase -> createDb($command); 
		}
		//Delete an existing database and get the name from the user and call deleteDb() function
		else if(fnmatch("DELETE,DATABASE,*", $line)) {
			$pieces = explode(',', $line);
			$command = array_pop($pieces);
			print_r($command);
			$createdDatabase -> deleteDB($command);
		}
		//Create a table to an existing database and get the table's name from the user and call addTable() function
		else if(fnmatch("CREATE,TABLE,*,COLUMNS,*", $line)) {
			$pieces = explode(',', $line);
			$createdDatabase -> addTable($pieces);
		}
		//Add a row to an existing table and get the column input from the user
		else if(preg_match('/ADD,.+/', $line, $matches)) {
			$pieces = explode(',', $line);
			print_r($pieces);
			$createdDatabase -> addRecord($pieces);
		}
		//Retrive and display rows that matches the primary key inserted by the user
		else if(preg_match('/GET,.+/', $line)) {
			$pieces = explode(',', $line);
			$createdDatabase -> getRecord($pieces);
		}
		//Delete the row in an existing table that matches the primary key 
		else if(preg_match('/DELETE,ROW,.+/', $line)) {
			$pieces = explode(',', $line);
			$rowNumber = str_replace("\"", "",$pieces[2]);
			print_r($rowNumber . "\n");
			if(is_numeric($rowNumber)){
				echo "is a number";
				$createdDatabase -> deleteRecord($rowNumber);
			}
			//The input from the user was not a number
			else {
				print_r("Integer PARAMETER is need, please enter an integer! \n");
			}
		}
		//User input didn't match existing command
		else {
			print_r("Not a valid command, please enter a valid command! \n");
		}

		$line = readline("Commands \n ");

	}

		


	


?>