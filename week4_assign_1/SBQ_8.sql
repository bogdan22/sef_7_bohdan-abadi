#the total and average values of rentals per month per year per store

USE sakila;

SELECT 
    ST.store_id,
    YEAR(P.payment_date) AS year,
    MONTH(P.payment_date) AS month,
    SUM(P.amount) AS sum,
    AVG(P.amount) AS Avereage
FROM
    payment AS P
        JOIN
    staff AS S ON P.staff_id = S.staff_id
        JOIN
    store AS ST ON ST.store_id = S.store_id
GROUP BY year , month , ST.store_id;
