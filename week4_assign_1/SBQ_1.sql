#total number of movies per actor

Use sakila;

SELECT 
    CONCAT(A.first_name, ' ', A.last_name) AS 'ACTOR',
    COUNT(FA.film_id) AS '# Of Movies'
FROM
    actor AS A
        JOIN
    film_actor AS FA ON A.actor_id = FA.actor_id
GROUP BY A.actor_id;
