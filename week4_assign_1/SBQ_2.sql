#top 3 languages for movies released in 2006

USE sakila;

SELECT Lan.name AS 'Language Name',
	   COUNT(Lan.language_id) AS 'Language Count'
From 
	language AS Lan
	JOIN film AS F ON Lan.language_id = F.language_id
GROUP BY Lan.language_id
ORDER BY `Language Count` DESC
LIMIT 3;
