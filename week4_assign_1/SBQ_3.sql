#top 3 countries from which customers are originating

USE sakila;

SELECT cl.country, Count(cl.country) AS 'Count Country'
	From customer_list AS cl 
GROUP BY cl.country
ORDER BY `Count Country` DESC
LIMIT 3;
