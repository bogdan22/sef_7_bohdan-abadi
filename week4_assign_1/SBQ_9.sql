#top 3 customers who rented the highest number of movies

USE sakila;

SELECT C.first_name, COUNT(R.rental_id) AS '# OF Movies'
FROM 
customer as C
Join rental as R ON C.customer_id = R.customer_id

GROUP BY C.first_name
ORDER BY COUNT(R.rental_id) DESC
LIMIT 3; 
