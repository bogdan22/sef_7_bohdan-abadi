#all the addresses where the second address is not empty

USE sakila;

SELECT 
    address2
FROM
    address
WHERE
    address2 !=''
ORDER BY address2;
