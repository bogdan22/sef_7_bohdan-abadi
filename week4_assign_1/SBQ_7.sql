#Find the names (first and last) of all the actors and 
#costumers whose first name is the same as the firstname of the actor with ID 8

USE sakila;

SELECT 
    CONCAT(A.first_name, ' ', A.last_name)
FROM
    actor AS A,
    actor AS AC
WHERE
    AC.actor_id = '8'
        AND A.first_name = AC.first_name
        AND A.actor_id != 8 
UNION ALL SELECT 
    CONCAT(C.first_name, ' ', C.last_name)
FROM
    customer AS C,
    actor AS AC
WHERE
    AC.actor_id = '8'
        AND C.first_name = AC.first_name;   
