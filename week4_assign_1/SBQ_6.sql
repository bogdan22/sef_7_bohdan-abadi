#Find all the film categories in which there are between 55 and 65 films

USE sakila;

SELECT 
    cat.name, COUNT(catF.film_id) AS '# of Films'
FROM
    category AS cat,
    film_category AS catF
WHERE
    cat.category_id = catF.category_id
GROUP BY cat.name
HAVING COUNT(catF.film_id) Between 55 AND 65 ;      

