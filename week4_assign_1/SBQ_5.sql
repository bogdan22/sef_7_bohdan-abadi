#Return the first and last names of actors who played in a film involving a “Crocodile” and a “Shark”

USE sakila;

SELECT 
    CONCAT(A.first_name, ' ', A.last_name) AS 'ACTOR',
    F.release_year
FROM
    actor AS A
        JOIN
    film_actor AS FA ON A.actor_id = FA.actor_id
        JOIN
    film AS F ON FA.film_id = F.film_id
WHERE
    F.description LIKE '%Crocodile%'
        AND F.description LIKE '%Shark%'
ORDER BY `ACTOR`;
