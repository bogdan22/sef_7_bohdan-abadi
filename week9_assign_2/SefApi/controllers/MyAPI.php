<?php
require_once 'API.php';
require_once 'mysqlWrapper.php';
require_once 'configuration.php';
class MyAPI extends API
{
    //protected $User;

    public function __construct($request) {
        parent::__construct($request);

        // Abstracted out for example
        // $APIKey = new Models\APIKey();
        // $User = new Models\User();

        // if (!array_key_exists('apiKey', $this->request)) {
        //     throw new Exception('No API Key provided');
        // } else if (!$APIKey->verifyKey($this->request['apiKey'], $origin)) {
        //     throw new Exception'(Invalid API Key');
        // } else if (array_key_exists('token', $this->request) &&
        //      !$User->get('token', $this->request['token'])) {

        //     throw new Exception('Invalid User Token');
        // }

        // $this->User = $User;
    }

    protected function getTableName() 
    {
      $table;
      if($this->method == 'GET' || $this->method == 'PUT' || $this->method == 'DELETE'){
        $table = $_GET['table'];
      }
      else{
        $table = $_POST['table'];
      }
      return $table;
    }


    protected function getColumnFilters() 
    {
      $inputs;
      if($this->method == 'GET' || $this->method == 'PUT' || $this->method == 'DELETE'){
        $inputs = $_GET;
        array_shift($inputs);
        array_shift($inputs);
      }
      else{
        $inputs = $_POST;
        array_shift($inputs);
        
      }
      return $inputs;
    }


    protected function getQueryBuilder()
    {   
            $table = $this->getTableName();
            $queryStr = 'SELECT * FROM sakila.' . $table;
            $filters = $this->getColumnFilters();
            $params = array();
              foreach ($filters as $key => $value) {
                $params[] = $key . ' = "' . $value . '"';
                $filters = ' WHERE ' . implode(' AND ', $params);
               
              }
            print_r($queryStr . $filters);
            return $queryStr . $filters;
    }

    protected function createQueryBuilder()
    {
      $table = $this->getTableName();
          $queryStr = 'INSERT INTO sakila.' . $table;
          $filters = $this->getColumnFilters();
          $columnName = array();
          $columnValues = array();
            foreach ($filters as $key => $value) {
              $columnsName[] = $key; 
              $columnsValues[] = "'". $value . "'";
              $name = ' ( ' . implode(' , ', $columnsName) . ')';
              $values = ' ( ' . implode(' , ', $columnsValues) . ')';
            }
          return $queryStr . $name . ' VALUES ' . $values;
    }

    protected function updateQueryBuilder()
    {
      $table = $this->getTableName();
          $queryStr = 'UPDATE sakila.' . $table . ' SET';
          $filters = $this->getColumnFilters();
          $params = array();

            foreach ($filters as $key => $value) {
              $params[] = $key . ' = "' . $value . '"';
              $filters = ' WHERE ' . implode(' AND ', $params);
            }
          //print_r($params);
          $file = urldecode($this->file);
          $varX = str_replace('&', '" , ', $file);
          $values = str_replace('=', '= "', $varX);
          return $queryStr . $values . $filters;
    }

    protected function deleteQueryBuilder()
    {   
            $table = $this->getTableName();
            $queryStr = 'DELETE * FROM sakila.' . $table;
            $filters = $this->getColumnFilters();
            $params = array();
              foreach ($filters as $key => $value) {
                $params[] = $key . ' = "' . $value . '"';
                $filters = ' WHERE ' . implode(' AND ', $params);
               
              }
            return $queryStr . $filters;
    }


    /**
     * Example of an Endpoint
     */
     protected function read($request) 
     {
        if ($this->method == 'GET') {
          $db = new database(DB_HOST,DB_USER, DB_PASSWORD, DB_DATABASE );
          
          if($db->query($this->getQueryBuilder())){
            return json_encode($db->results(), 128);
          }
          else{
            return $db->error();
          }
        } else {
            return "Only accepts GET requests";
        }
     }

     protected function create($request) 
     {
      if ($this->method == 'POST') {
          $db = new database(DB_HOST,DB_USER, DB_PASSWORD, DB_DATABASE );
          
          if($db->query($this->createQueryBuilder())){
            return json_encode($db->results(), 128);
          }
          else{
            return $db->error();
          }
     }

      else {
            return "Only accepts POST requests";
        }
      }

       protected function update($request) 
     {
      if ($this->method == 'PUT') {
          $db = new database(DB_HOST,DB_USER, DB_PASSWORD, DB_DATABASE );
          if($db->query($this->updateQueryBuilder())){
            return json_encode($db->results(), 128);
          }
          else{
            return $db->error();
          }
      }
      else {
            return "Only accepts POST requests";
        }
      }
      protected function delete($request) 
     {
        if ($this->method == 'DELETE') {
          $db = new database(DB_HOST,DB_USER, DB_PASSWORD, DB_DATABASE );
          if($db->query($this->deleteQueryBuilder())){
            return json_encode($db->results(), 128);
          }
          else{
            return $db->error();
          }
        } else {
            return "Only accepts GET requests";
        }
     }
    }
